from random import randint
import guesses

name = input("What is your name? ")

lowMonth = 1
highMonth = 12

lowYear = 1924
highYear = 2004

for x in range(1, 6):
    # guess the birthday of the player
    monthGuess = randint(lowMonth,highMonth)
    yearGuess = randint(lowYear,highYear)

    print("Guess", x, name, "Is your birthday", monthGuess, "/", yearGuess, "?")

    isComputerRight = input("yes or no? ")

    if isComputerRight == "Yes":
        print("I knew it")
        exit()
    elif x == 5:
        print("I have other things to do. Good bye.")
    else:
        if yearGuess != lowYear & highYear:
            print("Was my year too low or high or equal")
            yearResponse = input("? ")

        if monthGuess != lowMonth & highMonth:
            print("Was my month too low or high or equal")
            monthResponse = input("? ")

        highYear = guesses.Year_Guess(yearResponse,yearGuess,highYear,lowYear)[0]
        lowYear = guesses.Year_Guess(yearResponse,yearGuess,highYear,lowYear)[1]

        highMonth = guesses.Month_Guess(monthResponse,monthGuess,highMonth,lowMonth)[0]
        lowMonth = guesses.Month_Guess(monthResponse,monthGuess,highMonth,lowMonth)[1]
