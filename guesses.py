def Month_Guess(monthResponse, monthGuess, highMonth, lowMonth):
    if monthResponse == "high":
        highMonth = monthGuess - 1
    elif monthResponse == "low":
        lowMonth = monthGuess + 1
    elif monthResponse == "equal":
        highMonth = monthGuess
        lowMonth = monthGuess
    else:
        print("Fine, don't answer me")
    return highMonth, lowMonth

def Year_Guess(yearResponse, yearGuess, highYear, lowYear):
    if yearResponse == "high":
        highYear = yearGuess - 1
    elif yearResponse == "low":
        lowYear = yearGuess + 1
    elif yearResponse == "equal":
        highYear = yearGuess
        lowYear = yearGuess
    else:
        print("Fine, don't answer me")
    return highYear, lowYear
